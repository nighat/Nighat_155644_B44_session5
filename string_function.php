<?php
//Addslashes
echo "How Are\"You\" ";
echo "<br>";

  $myStr= addslashes('Hello "37" ');
echo $myStr;
echo "<br>";
$myStr= addslashes('Hello "37"Hello "37"Hello "37"Hello "37"Hello "37" ');
echo $myStr;
echo "<br>";
$myStr= addslashes("Hello word o'Really?");
echo $myStr;
echo "<br>";
//Explode  PHP
$myStr = "Hello World! How is life?<br>";
$myArr = explode(" ",$myStr);
print_r($myArr);
echo "<br>";
$myStr = implode(" ",$myArr);
print_r($myStr);
echo "<br>";
$myStr = implode(" #*",$myArr);
print_r($myStr);
echo "<br>";

//htmlentities
$myStr = '<br> Means Line Break';
$myStr = htmlentities($myStr);
echo $myStr;
echo "<br>";

//Trim function view page sourece
$myStr= "        Hello World ";
echo trim($myStr);
echo "<br>";
$myStr= "        Hello World ";
echo ltrim($myStr);
echo "<br>";
$myStr= "        Hello World ";
echo rtrim($myStr);
echo "<br>";

//$myStr = "\n\n\n\n\nHi There!";
//echo nl2br($myStr);
$mainStr = "Hello World";
$padStr = Str_pad($mainStr,50,"*");
echo $padStr;
echo "<br>";

$padStr = Str_pad($mainStr,50,$mainStr);
echo $padStr;
echo "<br>";

$repeatedStr = Str_repeat($mainStr,5);
echo $repeatedStr;
echo "<br>";

$mainStr = "Hello World";
$replacedStr = Str_replace("o","O",$mainStr);
echo $replacedStr;


echo "<br>";
$mainStr = "Hello World";
$myArr = Str_split($mainStr);
print_r($myArr);
echo "<br>";

echo strlen($mainStr);
echo "<br>";

echo strtoupper($mainStr);
echo "<br>";

$mainStr = "Hello World";
$substr = "Hello";
echo substr_compare($mainStr,$substr,0);
echo "<br>";

$mainStr = "Hello World";
$substr = "Hello World Hi Hi";
echo substr_compare($mainStr,$substr,0);
echo "<br>";

$substr = "Hello World";
echo substr_compare($mainStr,$substr,6);
echo "<br>";


$mainStr = "Hello World Hello Hello World ";
$substr = "Hi";
echo substr_count($mainStr,"Hello");
echo "<br>";

echo substr_replace($mainStr,$substr,7);
echo "<br>";

$mainStr = "hello world";
echo ucfirst($mainStr);
echo "<br>";

echo ucwords($mainStr);
echo "<br> <br>";
echo nl2br("One line.\n Another line.")

?>